﻿using UnityEngine;

namespace Assets.Scripts.Game.Trains
{
    public class Train : MonoBehaviour
    {
        public void DisableTrain()
        {
            gameObject.SetActive(false);
        }
    }
}
