﻿using Assets.Scripts.Game.Trains;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class TrainManager : MonoBehaviour
    {
        public Train Train;
        public float TimeBetweenTrains = 60f;
        public float TimeTrainArrive = 7f;
        public AudioSource TrainSoundSource;

        public void Activate()
        {
            StopAllCoroutines();
            StartCoroutine(LaunchTrainsRoutine());
        }

        private IEnumerator LaunchTrainsRoutine()
        {
            while (true)
            {
                if (Random.Range(0, 2) == 0)
                    StartCoroutine(LaunchTrain());
                yield return new WaitForSeconds(TimeBetweenTrains);
            }
        }

        private IEnumerator LaunchTrain()
        {
            yield return new WaitForSeconds(0.1f);
            TrainSoundSource.Play();
            Train.gameObject.SetActive(true);
        }
    }
}
