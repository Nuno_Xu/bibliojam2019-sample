﻿using Assets.Scripts.Game.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class ResourceManager
    {
        private Dictionary<ResourceType, int> _resourceAmount = new Dictionary<ResourceType, int>()
        {
            // Starting values
            { ResourceType.Comfort, 100 },
            { ResourceType.Food, 100 },
            { ResourceType.Money, 400 },
        };

        private Dictionary<ResourceType, int> _incomeAmount = new Dictionary<ResourceType, int>()
        {
            // Starting values
            { ResourceType.Comfort, 0 },
            { ResourceType.Food, 0 },
            { ResourceType.Money, 0 },
        };
        private Dictionary<ResourceType, int> _maxAmount = new Dictionary<ResourceType, int>()
        {
            // Max Resource Values
            { ResourceType.Comfort, 100 },
            { ResourceType.Food, 100 },
        };


        private Dictionary<ResourceType, List<Action>> _resourceCallbacks = new Dictionary<ResourceType, List<Action>>();
        private Dictionary<ResourceType, List<Action>> _incomeCallbacks = new Dictionary<ResourceType, List<Action>>();


        public Action RegisterValueCallback(ResourceType type, Action callback)
        {
            return RegisterCallback(_resourceCallbacks, type, callback);
        }

        public Dictionary<ResourceType, int> GetAllResource()
        {
            return new Dictionary<ResourceType, int>(_resourceAmount);
        }

        public Action RegisterIncomeCallback(ResourceType type, Action callback)
        {
            return RegisterCallback(_incomeCallbacks, type, callback);
        }

        public void AddResource(ResourceType type, int value)
        {
            var finalValue = _resourceAmount[type] + value;
            SetResource(type, finalValue);
        }

        public void SetResource(ResourceType type, int value)
        {
            if (_maxAmount.ContainsKey(type))
            {
                value = Mathf.Min(value, _maxAmount[type]);
            }
            _resourceAmount[type] = value;
            CallCallbacks(_resourceCallbacks, type);
        }

        public int GetResource(ResourceType type)
        {
            return _resourceAmount[type];
        }

        public int GetMaxResource(ResourceType type)
        {
            return _maxAmount[type];
        }


        public void SetIncome(ResourceType type, int value)
        {
            _incomeAmount[type] = value;
            CallCallbacks(_incomeCallbacks, type);
        }

        public int GetIncome(ResourceType type)
        {
            return _incomeAmount[type];
        }

        public void AddIncome()
        {
            foreach (KeyValuePair<ResourceType, int> incomePair in _incomeAmount)
            {
                if (incomePair.Value != 0)
                {
                    AddResource(incomePair.Key, incomePair.Value);
                }
            }

        }

        public bool TryPayCost(Dictionary<ResourceType, int> costDictionary)
        {
            if (!HasEnough(costDictionary))
                return false;

            foreach (KeyValuePair<ResourceType, int> costPair in costDictionary)
            {
                if (costPair.Value != 0)
                {
                    AddResource(costPair.Key, -costPair.Value);
                }
            }

            return true;
        }

        public bool HasEnough(Dictionary<ResourceType, int> costDictionary)
        {
            foreach (KeyValuePair<ResourceType, int> costPair in costDictionary)
            {
                var type = costPair.Key;
                if (_resourceAmount[type] < costPair.Value)
                {
                    return false;
                }
            }
            return true;
        }

        public void UpdateIncome(IEnumerable<MapTile> levelMap)
        {
            if (levelMap == null)
                return;

            Dictionary<ResourceType, int> income = new Dictionary<ResourceType, int>();
            foreach (MapTile mapTile in levelMap)
            {
                var tileIncome = mapTile.GetIncome();
                if (tileIncome != null)
                {
                    foreach (KeyValuePair<ResourceType, int> incomePair in tileIncome)
                    {
                        var type = incomePair.Key;
                        if (!income.ContainsKey(type))
                            income[type] = 0;
                        income[type] += incomePair.Value;
                    }
                }
            }

            foreach (ResourceType type in _incomeAmount.Keys.ToArray())
            {
                int newIncome = 0;
                if (income.ContainsKey(type))
                {
                    newIncome = income[type];
                }
                SetIncome(type, newIncome);
            }
            foreach (KeyValuePair<ResourceType, int> incomePair in income)
            {
                SetIncome(incomePair.Key, incomePair.Value);
            }
        }

        private void CallCallbacks<T>(Dictionary<T, List<Action>> _callbackDictionary, T type)
        {
            if (_callbackDictionary.ContainsKey(type))
            {
                var callbackList = _callbackDictionary[type];
                foreach (Action callback in callbackList)
                {
                    callback?.Invoke();
                }
            }
        }

        private Action RegisterCallback<T>(Dictionary<T, List<Action>> _callbackDictionary, T type, Action callback)
        {
            if (!_callbackDictionary.ContainsKey(type))
                _callbackDictionary.Add(type, new List<Action>());

            var resourceList = _callbackDictionary[type];
            resourceList.Add(callback);
            callback?.Invoke();
            return () => resourceList.Remove(callback);
        }
    }
}
