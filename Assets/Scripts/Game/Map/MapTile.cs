﻿using Assets.Scripts.Cam;
using Assets.Scripts.UI.Game;
using Assets.Scripts.Util;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Game.Map
{
    public enum TileType
    {
        Empty,
        Money,
        Food,
        Comfort,
        School
    }


    [Serializable]
    public struct TileTypeSpriteData
    {
        public TileType Type;
        public Phase Phase;
        public Sprite Sprite;
    }

    [ExecuteInEditMode]
    public class MapTile : MonoBehaviour
    {
        public SpriteRenderer TileSpriteRenderer;
        public TileTypeSpriteData[] TileTypeSettingsArray;

        public Color NormalColor = Color.white;
        public Color HighlightColor = Color.white;
        public ObjectToggler HelpMessage;
        private bool _click = false;


        [SerializeField]
        private TileType _tileType;
        public TileType TileType
        {
            get { return _tileType; }
            set
            {
                _tileType = value;
                SetTileTypeSprite();
            }
        }

        [SerializeField]
        private Phase _phase;
        public Phase Phase
        {
            get { return _phase; }
            set
            {
                _phase = value;
                SetTileTypeSprite();
            }
        }

        private void SetTileTypeSprite()
        {
            if (TileTypeSettingsArray == null)
                return;

            foreach (TileTypeSpriteData resourceTypeIcon in TileTypeSettingsArray)
            {
                if (resourceTypeIcon.Type == _tileType && (resourceTypeIcon.Phase == Phase.All || resourceTypeIcon.Phase == _phase))
                {
                    TileSpriteRenderer.sprite = resourceTypeIcon.Sprite;
                    break;
                }
            }
        }

        public Dictionary<ResourceType, int> GetIncome()
        {
            return LevelManager.Instance.GetTileTypeIncome(_tileType, _phase);
        }

        private void OnMouseOver()
        {
            if (!EventSystem.current.IsPointerOverGameObject() && TutorialValid())
                TileSpriteRenderer.color = HighlightColor;
            else
                TileSpriteRenderer.color = NormalColor;
        }

        private void OnMouseExit()
        {
            TileSpriteRenderer.color = NormalColor;
            _click = false;
        }

        private void OnMouseUp()
        {
            if (_click && !CameraMove.Instance.SignificantDrag && !EventSystem.current.IsPointerOverGameObject() && TutorialValid())
            {
                if (HelpMessage != null)
                    HelpMessage.gameObject.SetActive(false);

                if (TileType == TileType.Empty)
                    BuildingSelector.Instance.OpenSelector(this);
                else
                    BuildingSell.Instance.OpenSelector(this);
                _click = false;
            }
        }

        private void OnMouseDown()
        {
            _click = true;
        }

        private bool TutorialValid()
        {
            return !LevelManager.Instance.TutorialActive || _tileType == TileType.Empty;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                SetTileTypeSprite();
            }
        }
#endif
    }
}
