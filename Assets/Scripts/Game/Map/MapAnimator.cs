﻿using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.Game.Map
{
    public class MapAnimator : SingletonMonoBehaviour<MapAnimator>
    {
        public const string SHAKING = "Shaking";
        public Animator Animator;


        public void SetAnimation(string triggerName, bool triggerValue)
        {
            Animator.SetBool(triggerName, triggerValue);
        }
    }
}
