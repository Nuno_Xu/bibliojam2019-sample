﻿using Assets.Scripts.Cam;
using Assets.Scripts.UI.Game;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Game
{
    [ExecuteInEditMode]
    public class TowerUpdater : MonoBehaviour
    {
        [Serializable]
        public struct TowerSpriteData
        {
            public Phase Phase;
            public Sprite Sprite;
        }

        [SerializeField]
        private Phase _phase;
        public Phase Phase
        {
            get { return _phase; }
            set
            {
                _phase = value;
                SetTowerSprite();
            }
        }



        public TowerSpriteData[] TowerSpriteArray;
        public SpriteRenderer TowerSpriteRenderer;

        public Color NormalColor = Color.white;
        public Color HighlightColor = Color.white;
        private bool _click = false;

        private void SetTowerSprite()
        {
            if (TowerSpriteArray == null)
                return;

            foreach (TowerSpriteData towerSprite in TowerSpriteArray)
            {
                if (towerSprite.Phase == _phase)
                {
                    TowerSpriteRenderer.sprite = towerSprite.Sprite;
                    break;
                }
            }
        }

        private void OnMouseOver()
        {
            if (!EventSystem.current.IsPointerOverGameObject() && TutorialValid())
                TowerSpriteRenderer.color = HighlightColor;
            else
                TowerSpriteRenderer.color = NormalColor;
        }

        private void OnMouseExit()
        {
            TowerSpriteRenderer.color = NormalColor;
            _click = false;
        }

        private void OnMouseUp()
        {
            if (_click && !CameraMove.Instance.SignificantDrag && !EventSystem.current.IsPointerOverGameObject() && TutorialValid())
            {
                PhaseBuyPanel.Instance.OpenPanel();
                _click = false;
            }
        }

        private void OnMouseDown()
        {
            _click = true;
        }


        private bool TutorialValid()
        {
            return !LevelManager.Instance.TutorialActive;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                SetTowerSprite();
            }
        }
#endif
    }
}
