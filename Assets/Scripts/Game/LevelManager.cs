﻿using Assets.Scripts.Game.Map;
using Assets.Scripts.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public enum ResourceType
    {
        Money,
        Food,
        Comfort
    }


    public enum Phase
    {
        Past,
        Present,
        Future,
        All
    }


    [Serializable]
    public struct TileTypeCostData
    {
        public TileType Type;
        public Phase Phase;
        public ResourceType ResourceType;
        public int Cost;
    }

    [Serializable]
    public struct TileTypeIncomeData
    {
        public TileType Type;
        public Phase Phase;
        public ResourceType ResourceType;
        public int Income;
    }

    public class LevelManager : SingletonMonoBehaviour<LevelManager>
    {
        public bool TutorialActive;
        public bool Endless;

        public List<MapTile> LevelMap;
        public ResourceManager ResourceManager = new ResourceManager();
        public TileTypeIncomeData[] IncomeDataArray;
        public TileTypeCostData[] TileTypeCostArray;
        public Phase Phase;

        public float SellRatio = 0.75f;

        private WaitForSeconds p__waitABit;
        private WaitForSeconds _waitABit
        {
            get
            {
                if (p__waitABit == null)
                    p__waitABit = new WaitForSeconds(0.2f);
                return p__waitABit;
            }
        }


        private float _lastIncomeTime = float.MinValue;

        private void Start()
        {
            LevelMap = new List<MapTile>(FindObjectsOfType<MapTile>());
            OrderMap();
            UpdateIncome();

            StartCoroutine(IncomeRoutine());
        }

        private IEnumerator IncomeRoutine()
        {
            while (true)
            {
                yield return _waitABit;
                if (Time.time - _lastIncomeTime > 1f)
                {
                    ResourceManager.AddIncome();
                    _lastIncomeTime = Time.time;
                }
            }
        }

        public void UpdateIncome()
        {
            ResourceManager.UpdateIncome(LevelMap);
        }

        public void OrderMap()
        {
            LevelMap.Sort((MapTile left, MapTile right) =>
            {
                var xDif = Mathf.RoundToInt((right.transform.position.x - left.transform.position.x) * 100);
                var yDif = Mathf.RoundToInt((right.transform.position.y - left.transform.position.y) * 100);

                return yDif * 100 + xDif;

            });

            int index = 0;
            foreach (MapTile tile in LevelMap)
            {
                tile.TileSpriteRenderer.sortingOrder = index;

                index++;
            }
        }


        public Dictionary<ResourceType, int> GetTileTypeCost(TileType tileType, Phase phase)
        {
            if (TileTypeCostArray == null)
                return null;

            Dictionary<ResourceType, int> cost = new Dictionary<ResourceType, int>();
            foreach (TileTypeCostData costData in TileTypeCostArray)
            {
                if (costData.Type == tileType && (costData.Phase == Phase.All || costData.Phase == phase))
                {
                    cost.Add(costData.ResourceType, costData.Cost);
                }
            }

            return cost;
        }



        public Dictionary<ResourceType, int> GetTileTypeSell(TileType tileType, Phase phase)
        {
            if (TileTypeCostArray == null)
                return null;

            Dictionary<ResourceType, int> cost = new Dictionary<ResourceType, int>();
            foreach (TileTypeCostData costData in TileTypeCostArray)
            {
                if (costData.Type == tileType && (costData.Phase == Phase.All || costData.Phase == phase))
                {
                    cost.Add(costData.ResourceType, Mathf.CeilToInt(costData.Cost * SellRatio));
                }
            }

            return cost;
        }


        public Dictionary<ResourceType, int> GetTileTypeIncome(TileType tileType, Phase phase)
        {
            if (IncomeDataArray == null)
                return null;

            Dictionary<ResourceType, int> income = new Dictionary<ResourceType, int>();
            foreach (TileTypeIncomeData incomeData in IncomeDataArray)
            {
                if (incomeData.Type == tileType && (incomeData.Phase == Phase.All || incomeData.Phase == phase))
                {
                    income.Add(incomeData.ResourceType, incomeData.Income);
                }
            }

            return income;
        }
    }
}

