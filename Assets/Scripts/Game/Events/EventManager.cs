﻿using Assets.Scripts.Util;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Game.Events
{
    public class EventManager : SingletonMonoBehaviour<EventManager>
    {
        public const string WIN_EVENT = "Win";
        public const string NO_MONEY_EVENT = "NoMoney";
        public const string MUST_BUILD_FACTORY = "MustBuildFactory";
        public const string EARTHQUAKE = "Earthquake";


        [HideInInspector]
        public bool ResolvingEvent = false;
        private LevelEvent[] _eventArray;
        public EventModal PanelToggler;

        private void Start()
        {
            _eventArray = GetComponentsInChildren<LevelEvent>();
        }

        private void Update()
        {
            if (!ResolvingEvent && _eventArray != null)
            {
                foreach (LevelEvent levelEvent in _eventArray)
                {
                    if (levelEvent.CheckTrigger())
                    {
                        ShowEvent(levelEvent);
                        break;
                    }
                }
            }
        }

        public void TriggerEventDelayed(string eventName, float timer)
        {
            StartCoroutine(TriggerEventDelayedRoutine(eventName, timer));
        }

        private IEnumerator TriggerEventDelayedRoutine(string eventName, float timer)
        {
            yield return new WaitForSeconds(timer);
            TriggerEvent(eventName);
        }

        public void TriggerEvent(string eventName)
        {
            if (!ResolvingEvent && _eventArray != null)
            {
                foreach (LevelEvent levelEvent in _eventArray)
                {
                    if (levelEvent.EventName == eventName)
                    {
                        ShowEvent(levelEvent);
                        break;
                    }
                }
            }
        }

        private void ShowEvent(LevelEvent levelEvent)
        {
            Time.timeScale = 0f;
            ResolvingEvent = true;
            levelEvent.Triggered = true;
            PanelToggler.ShowEvent(levelEvent, HandleEventFinished);
        }

        private void HandleEventFinished()
        {
            ResolvingEvent = false;
            Time.timeScale = 1f;
        }
    }
}
