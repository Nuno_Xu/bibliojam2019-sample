﻿using Assets.Scripts.Game.Events.EventDecisions;
using Assets.Scripts.Game.Events.EventEffects;
using Assets.Scripts.Game.Events.EventTriggers;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.Events
{
    public class LevelEvent : MonoBehaviour
    {
        public string EventName;
        public bool SimpleEvent;
        public bool OneShot;
        public bool FinalEvent;

        [TextArea]
        public string[] EventText;
        [TextArea]
        public string ConsequencesText;
        [TextArea]
        public string HintText;
        public Sprite EventAvatar;

        [HideInInspector]
        public bool ShowResourceConsequences;
        [HideInInspector]
        public Dictionary<ResourceType, int> PreviousResources;

        public EventTrigger EventTrigger;
        public EventEffect[] EventPreEffectArray;
        public EventEffect[] EventEffectArray;
        public EventDecision[] EventDecisionArray;
        public Phase EventMinPhase;

        [HideInInspector]
        public bool Triggered = false;

        public bool CheckTrigger()
        {
            if (OneShot && Triggered)
                return false;

            //if (EventMinPhase != Phase.All && LevelManager.Instance.Phase < EventMinPhase)
            //  return false;

            if (EventTrigger != null)
                return EventTrigger.CheckTrigger();
            else
                return false;
        }

        public bool HasConsequence()
        {
            return ShowResourceConsequences || !string.IsNullOrEmpty(ConsequencesText);
        }


        public void TriggerPreEffect()
        {
            if (EventPreEffectArray != null)
            {
                foreach (EventEffect eventEffect in EventPreEffectArray)
                {
                    if (eventEffect != null)
                        eventEffect.TriggerEffect(this);
                }
            }
        }


        public void TriggerEffect()
        {
            ShowResourceConsequences = false;
            PreviousResources = LevelManager.Instance.ResourceManager.GetAllResource();
            if (EventEffectArray != null)
            {
                foreach (EventEffect eventEffect in EventEffectArray)
                {
                    if (eventEffect != null)
                        eventEffect.TriggerEffect(this);
                }
            }
        }

        public bool HasHint()
        {
            return !string.IsNullOrEmpty(HintText);
        }

        public bool HasDecision()
        {
            return EventDecisionArray != null && EventDecisionArray.Length > 0;
        }
    }
}
