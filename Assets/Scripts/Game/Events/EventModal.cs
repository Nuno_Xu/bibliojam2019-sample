﻿using Assets.Scripts.UI;
using Assets.Scripts.UI.Game;
using Assets.Scripts.Util;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game.Events
{
    public class EventModal : MonoBehaviour
    {
        public ObjectToggler PanelToggler;
        public Image FaderToggler;

        public Conversation Conversation;
        public ConsequencesPanel ConsequencesPanel;
        public HintPanel HintPanel;
        public WinPanel WinPanel;
        public LossPanel LossPanel;

        private LevelEvent _currentEvent;
        private Action _eventFinished;

        public Color NormalFaderColor = Color.white;
        public Color SimpleFaderColor = Color.white;

        public void ShowEvent(LevelEvent levelEvent, Action eventFinished)
        {
            PanelToggler.OpenPanel();
            _currentEvent = levelEvent;
            _eventFinished = eventFinished;
            ConsequencesPanel.gameObject.SetActive(false);
            HintPanel.gameObject.SetActive(false);
            WinPanel.gameObject.SetActive(false);
            LossPanel.gameObject.SetActive(false);

            if (levelEvent.SimpleEvent)
                FaderToggler.color = SimpleFaderColor;
            else
                FaderToggler.color = NormalFaderColor;

            Conversation.SetConversation(levelEvent.EventText, levelEvent.EventAvatar, HandleConversationFinish);
        }

        private void HandleConversationFinish()
        {
            if (_currentEvent.HasDecision())
            {

            }
            else
            {
                _currentEvent.TriggerEffect();
                if (_currentEvent.FinalEvent)
                {
                }
                else if (_currentEvent.HasConsequence())
                {
                    ConsequencesPanel.ShowConsequences(_currentEvent, HandleEventClose);
                }
                else if (_currentEvent.HasHint())
                {
                    HintPanel.ShowHint(_currentEvent, HandleEventClose);
                }
                else
                {
                    HandleEventClose();
                }
            }
        }


        public void HandleEventClose()
        {

            PanelToggler.ClosePanel();
            _eventFinished();
        }
    }
}
