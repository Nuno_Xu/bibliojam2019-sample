﻿using System;

namespace Assets.Scripts.Game.Events.EventTriggers
{
    public class EventTriggerResources : EventTrigger
    {
        [Serializable]
        public struct ResourceAmount
        {
            public ResourceType ResourceType;
            public int Amount;
        }

        public ResourceAmount[] LessThanResourceAmountArray;
        public ResourceAmount[] MoreThanResourceAmountArray;

        public override bool CheckTrigger()
        {
            if (LessThanResourceAmountArray == null && MoreThanResourceAmountArray == null)
                return false;

            var resourceManager = LevelManager.Instance.ResourceManager;
            foreach (ResourceAmount amount in LessThanResourceAmountArray)
            {
                var resourceType = amount.ResourceType;
                var currentValue = resourceManager.GetResource(resourceType);
                if (currentValue >= amount.Amount)
                {
                    return false;
                }
            }

            foreach (ResourceAmount amount in MoreThanResourceAmountArray)
            {
                var resourceType = amount.ResourceType;
                var currentValue = resourceManager.GetResource(resourceType);
                if (currentValue <= amount.Amount)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
