﻿namespace Assets.Scripts.Game.Events.EventTriggers
{
    public class EventTriggerLoss : EventTrigger
    {
        public ResourceType resourceType;
        private bool _lose = false;

        public override bool CheckTrigger()
        {
            if (_lose)
                return true;

            var resourceManager = LevelManager.Instance.ResourceManager;
            var targetValue = resourceManager.GetResource(resourceType);
            var targetIncome = resourceManager.GetIncome(resourceType);

            if (targetValue < 0)
                _lose = true;


            return false;
        }
    }
}
