﻿using UnityEngine;

namespace Assets.Scripts.Game.Events.EventTriggers
{
    public abstract class EventTrigger : MonoBehaviour
    {
        public abstract bool CheckTrigger();
    }
}
