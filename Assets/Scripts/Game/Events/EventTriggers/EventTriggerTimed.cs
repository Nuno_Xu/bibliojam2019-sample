﻿using UnityEngine;

namespace Assets.Scripts.Game.Events.EventTriggers
{
    public class EventTriggerTimed : EventTrigger
    {
        public float TimeToTrigger;
        public bool AfterTutorial;

        private float _startTime;
        private bool _started;
        private LevelEvent _levelEvent;

        private void Start()
        {
            _levelEvent = GetComponent<LevelEvent>();
        }

        public override bool CheckTrigger()
        {
            if (!_started)
            {
                if (AfterTutorial && LevelManager.Instance.TutorialActive)
                    return false;
                //if (_levelEvent != null && _levelEvent.EventMinPhase != Phase.All && LevelManager.Instance.Phase < _levelEvent.EventMinPhase)
                //  return false;

                _startTime = Time.time;
                _started = true;
            }

            return Time.time - _startTime > TimeToTrigger;
        }
    }
}
