﻿
using UnityEngine;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public abstract class EventEffect : MonoBehaviour
    {
        public abstract void TriggerEffect(LevelEvent levelEvent);
    }
}
