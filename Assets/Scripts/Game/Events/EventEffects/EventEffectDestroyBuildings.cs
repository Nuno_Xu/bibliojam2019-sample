﻿using Assets.Scripts.Game.Map;
using Assets.Scripts.Util;
using System.Collections.Generic;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectDestroyBuildings : EventEffect
    {
        public bool DestroyAllNotFactory;
        public int DestroyCount;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            if (DestroyAllNotFactory)
            {
                PerformDestroyAllNotFactory();
            }
            else
            {
                PerformDestroyCount(DestroyCount);
            }

            LevelManager.Instance.UpdateIncome();
        }

        private void PerformDestroyCount(int count)
        {
            List<MapTile> tileMap = new List<MapTile>(LevelManager.Instance.LevelMap);
            int destroyed = 0;
            tileMap.Shuffle();
            bool skippedMoney = false;
            foreach (var tile in tileMap)
            {
                if (tile.TileType != TileType.Empty)
                {
                    if (!skippedMoney && tile.TileType == TileType.Money)
                    {
                        skippedMoney = true;
                        continue;
                    }

                    tile.TileType = TileType.Empty;
                    destroyed++;
                    if (destroyed >= count)
                        return;
                }
            }
        }

        private void PerformDestroyAllNotFactory()
        {
            var tileMap = LevelManager.Instance.LevelMap;

            bool skippedMoney = false;
            foreach (var tile in tileMap)
            {
                if (!skippedMoney && tile.TileType == TileType.Money)
                {
                    skippedMoney = true;
                    continue;
                }
                tile.TileType = TileType.Empty;
            }
        }
    }
}
