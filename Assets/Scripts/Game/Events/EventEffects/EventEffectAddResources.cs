﻿using System;

namespace Assets.Scripts.Game.Events.EventEffects
{

    public class EventEffectAddResources : EventEffect
    {
        [Serializable]
        public struct EffectResourceData
        {
            public ResourceType ResourceType;
            public int ResourceAmount;
        }

        public EffectResourceData[] Effects;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            if (Effects != null)
            {
                var resourceManager = LevelManager.Instance.ResourceManager;
                foreach (EffectResourceData data in Effects)
                {
                    resourceManager.AddResource(data.ResourceType, data.ResourceAmount);
                }
            }

            levelEvent.ShowResourceConsequences = true;
        }
    }
}
