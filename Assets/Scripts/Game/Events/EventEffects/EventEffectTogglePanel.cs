﻿using Assets.Scripts.Util;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectTogglePanel : EventEffect
    {
        public ObjectToggler ObjectToggler;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            ObjectToggler.ToggleObject();
        }
    }
}
