﻿namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectTriggerOtherEvent : EventEffect
    {
        public string EventName;
        public float EventDelay;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            EventManager.Instance.TriggerEventDelayed(EventName, EventDelay);
        }
    }
}
