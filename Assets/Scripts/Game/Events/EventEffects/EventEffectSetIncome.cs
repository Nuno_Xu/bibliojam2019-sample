﻿using Assets.Scripts.UI.Game;
using System.Collections.Generic;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectSetIncome : EventEffect
    {
        public bool Replace;
        public bool Delta = true;
        public TileTypeIncomeData[] IncomeDataArray;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            if (Replace)
            {
                LevelManager.Instance.IncomeDataArray = IncomeDataArray;
            }
            else
            {
                var newIncomeList = new List<TileTypeIncomeData>();
                foreach (TileTypeIncomeData currentIncomeData in LevelManager.Instance.IncomeDataArray)
                {
                    bool foundNewData = false;
                    TileTypeIncomeData data = new TileTypeIncomeData();
                    foreach (TileTypeIncomeData newIncomeData in IncomeDataArray)
                    {
                        if (newIncomeData.ResourceType == currentIncomeData.ResourceType &&
                             newIncomeData.Phase == currentIncomeData.Phase &&
                              newIncomeData.Type == currentIncomeData.Type)
                        {
                            foundNewData = true;
                            data = newIncomeData;
                            if (Delta)
                                data.Income += currentIncomeData.Income;

                            break;
                        }
                    }

                    if (foundNewData)
                        newIncomeList.Add(data);
                    else
                        newIncomeList.Add(currentIncomeData);

                }
                LevelManager.Instance.IncomeDataArray = newIncomeList.ToArray();
            }

            if (BuildingSelector.Instance != null && BuildingSelector.Instance.BuildCardArray != null)
            {
                foreach (var card in BuildingSelector.Instance.BuildCardArray)
                {
                    card.SetTileEffectText();
                }
            }

            LevelManager.Instance.UpdateIncome();
        }
    }
}
