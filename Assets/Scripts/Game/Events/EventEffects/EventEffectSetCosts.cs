﻿using Assets.Scripts.UI.Game;
using System.Collections.Generic;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectSetCosts : EventEffect
    {
        public bool Replace;
        public TileTypeCostData[] CostDataArray;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            if (Replace)
            {
                LevelManager.Instance.TileTypeCostArray = CostDataArray;
            }
            else
            {
                var newIncomeList = new List<TileTypeCostData>();
                foreach (TileTypeCostData currentCostData in LevelManager.Instance.TileTypeCostArray)
                {
                    bool foundNewData = false;
                    TileTypeCostData data = new TileTypeCostData();
                    foreach (TileTypeCostData newCostData in CostDataArray)
                    {
                        if (newCostData.ResourceType == currentCostData.ResourceType &&
                             newCostData.Phase == currentCostData.Phase &&
                              newCostData.Type == currentCostData.Type)
                        {
                            foundNewData = true;
                            data = newCostData;
                            break;
                        }
                    }

                    if (foundNewData)
                        newIncomeList.Add(data);
                    else
                        newIncomeList.Add(currentCostData);

                }
            }
            if (BuildingSelector.Instance != null && BuildingSelector.Instance.BuildCardArray != null)
            {
                foreach (var card in BuildingSelector.Instance.BuildCardArray)
                {
                    card.SetTileCostText();
                }
            }
        }
    }
}
