﻿using Assets.Scripts.UI;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectWin : EventEffect
    {
        public WinPanel Panel;
        public override void TriggerEffect(LevelEvent levelEvent)
        {
            Panel.ShowWin();
        }
    }
}
