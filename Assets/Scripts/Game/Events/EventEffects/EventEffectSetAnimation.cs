﻿using Assets.Scripts.Game.Map;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectSetAnimation : EventEffect
    {
        public string TriggerName;
        public bool TriggerValue;

        public override void TriggerEffect(LevelEvent levelEvent)
        {
            MapAnimator.Instance.SetAnimation(TriggerName, TriggerValue);
        }
    }
}
