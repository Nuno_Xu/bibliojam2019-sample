﻿namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectActivateTrain : EventEffect
    {
        public TrainManager TrainManager;
        public override void TriggerEffect(LevelEvent levelEvent)
        {
            TrainManager.Activate();
        }
    }
}
