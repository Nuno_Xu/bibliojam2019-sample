﻿using System;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectSetResources : EventEffect
    {
        [Serializable]
        public struct ResourceAmount
        {
            public ResourceType ResourceType;
            public int Amount;
        }

        public ResourceAmount[] TargetResourceAmountArray;


        public override void TriggerEffect(LevelEvent levelEvent)
        {
            var resourceManager = LevelManager.Instance.ResourceManager;
            foreach (ResourceAmount resourceAmount in TargetResourceAmountArray)
            {
                resourceManager.SetResource(resourceAmount.ResourceType, resourceAmount.Amount);
            }

            levelEvent.ShowResourceConsequences = true;
        }
    }
}
