﻿using Assets.Scripts.UI;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectLoss : EventEffect
    {
        public LossPanel Panel;
        public override void TriggerEffect(LevelEvent levelEvent)
        {
            Panel.ShowLoss();
        }
    }
}
