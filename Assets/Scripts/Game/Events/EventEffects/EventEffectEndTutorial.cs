﻿namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectEndTutorial : EventEffect
    {
        public override void TriggerEffect(LevelEvent levelEvent)
        {
            LevelManager.Instance.TutorialActive = false;
        }
    }
}
