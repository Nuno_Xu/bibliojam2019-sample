﻿using Assets.Scripts.Game.Map;
using Assets.Scripts.Sound;
using Assets.Scripts.UI.Game;

namespace Assets.Scripts.Game.Events.EventEffects
{
    public class EventEffectNextPhase : EventEffect
    {
        public TowerUpdater Tower;
        public Phase NextPhase;
        public override void TriggerEffect(LevelEvent levelEvent)
        {

            foreach (MapTile tile in LevelManager.Instance.LevelMap)
            {
                tile.Phase = NextPhase;
            }

            if (BuildingSelector.Instance != null && BuildingSelector.Instance.BuildCardArray != null)
            {
                foreach (var card in BuildingSelector.Instance.BuildCardArray)
                {
                    card.Phase = NextPhase;
                }
            }

            if (Tower == null)
                Tower = FindObjectOfType<TowerUpdater>();

            Tower.Phase = NextPhase;
            PhaseBuyPanel.Instance.Phase = NextPhase;
            LevelManager.Instance.Phase = NextPhase;

            BackgroundMusicManager.Instance.SetPhase(NextPhase);
        }
    }
}
