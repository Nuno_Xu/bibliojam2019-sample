﻿using System;
using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game.Events
{
    public class Conversation : MonoBehaviour
    {
        public TMP_Text ConversationText;
        public Image PersonImage;
        public float TextSpeed = 0.2f;
        public Transform PressToContinuePanel;
        public AudioSource TalkAudio;


        private string[] _conversationTextArray;
        private int _currentIndex = -1;
        private Action _conversationEndedCallback;
        private bool _readyForNextLine = true;
        private bool _skip = false;

        public void SetConversation(string[] conversationArray, Sprite person, Action conversationEndedCallback)
        {
            _conversationTextArray = conversationArray;
            _conversationEndedCallback = conversationEndedCallback;
            _currentIndex = -1;
            _readyForNextLine = true;
            _skip = false;
            PressToContinuePanel.gameObject.SetActive(true);

            PersonImage.sprite = person;
            NextConversation();
        }

        public void ShowConversation(string conversation)
        {
            _readyForNextLine = false;
            _skip = false;
            StartCoroutine(WriteText(conversation));
        }


        public void SetReadyForNextLine()
        {
            _readyForNextLine = true;
        }

        public void NextConversation()
        {

            if (_readyForNextLine)
            {
                _currentIndex++;

                if (_currentIndex > _conversationTextArray.Length)
                    return;
                if (_currentIndex == _conversationTextArray.Length)
                {
                    PressToContinuePanel.gameObject.SetActive(false);
                    _conversationEndedCallback();
                    return;
                }

                var nextConversation = _conversationTextArray[_currentIndex];
                ShowConversation(nextConversation);
            }
            else
            {
                _skip = true;
            }
        }

        private IEnumerator WriteText(string text)
        {
            StringBuilder currentString = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                currentString.Append(text[i]);
                ConversationText.text = currentString.ToString();
                TalkAudio.Play();
                yield return new WaitForSecondsRealtime(TextSpeed);

                if (_skip)
                {
                    ConversationText.text = text;
                    break;
                }
            }

            SetReadyForNextLine();
        }
    }
}

