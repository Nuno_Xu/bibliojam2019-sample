﻿using UnityEngine;

namespace Assets.Scripts.Cam
{
    [ExecuteInEditMode]
    public class CameraController : MonoBehaviour
    {
        public Camera Camera;
        public SpriteRenderer Background;
        public float MinRatio = 0.6f;
        public AudioSource SeaAudioSource;

        private void Update()
        {
            SetCamera();
            SetCameraLimits();
        }

        public void SetCamera()
        {
            if (Background == null || Camera == null)
                return;

            var backgroundSize = Background.bounds.size;
            var cameraAspect = Camera.aspect;

            float targetHeight;
            targetHeight = backgroundSize.x / cameraAspect;

            Camera.orthographicSize = targetHeight / 2f;
        }

        public void SetCameraLimits()
        {
            var yPosition = transform.position.y;
            var mapYPosition = Background.transform.position.y;
            var orthoSize = Camera.orthographicSize;
            var yMax = mapYPosition + Background.bounds.extents.y - orthoSize;
            var yMin = mapYPosition - Background.bounds.extents.y + orthoSize;


            var newYPosition = yPosition;
            if (yPosition > yMax)
                newYPosition = yMax;
            else if (yPosition < yMin)
                newYPosition = yMin;

            if (newYPosition != yPosition)
            {
                var newPosition = transform.position;
                newPosition.y = newYPosition;
                transform.position = newPosition;
            }

            var total = yMax - yMin;
            if (total == 0)
                return;
            var yRatio = (transform.position.y - yMin) / (total);
            SetSeaVolume(yRatio);
        }

        private void SetSeaVolume(float yRatio)
        {
            if (yRatio >= MinRatio && MinRatio > 0)
            {
                SeaAudioSource.volume = (yRatio - (1 - MinRatio)) / MinRatio;
            }
            else
            {
                SeaAudioSource.volume = 0f;
            }
        }
    }
}
