﻿using Assets.Scripts.Util;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Cam
{
    public class CameraMove : SingletonMonoBehaviour<CameraMove>
    {
        public Camera Camera;
        public ContactFilter2D MoveFilter;
        private float _mouseInitY;

        public float SignificantDragThreshold = 1f;
        public bool SignificantDrag { get; private set; }


        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var worldPosition = Camera.ScreenToWorldPoint(Input.mousePosition);
                _mouseInitY = worldPosition.y;
                SignificantDrag = false;
            }
            else if (Input.GetMouseButtonUp(0))
            {
            }
            else if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                var worldPosition = Camera.ScreenToWorldPoint(Input.mousePosition);
                var yDelta = _mouseInitY - worldPosition.y;
                if (Mathf.Abs(yDelta) > SignificantDragThreshold)
                    SignificantDrag = true;
                transform.position = new Vector3(transform.position.x, transform.position.y + yDelta, transform.position.z);
            }
        }
    }
}
