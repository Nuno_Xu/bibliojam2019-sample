﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.Dialog
{
    public class DialogBubble : MonoBehaviour
    {
        public TMP_Text DialogueText;

        public void Delete()
        {
            Destroy(gameObject);
        }

        public void SetDialog(string dialogue, float timeLimit)
        {
            DialogueText.text = dialogue;
            StartCoroutine(SetTimeLimit(timeLimit));
        }

        private IEnumerator SetTimeLimit(float timeLimit)
        {
            yield return new WaitForSeconds(timeLimit);
            Delete();
        }
    }
}
