﻿using Assets.Scripts.Util;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UI.Dialog
{
    public class DialogList : SingletonMonoBehaviour<DialogList>
    {
        public DialogBubble DialogBubblePrefab;
        public float DialogTime = 1f;
        public float DialogLimit = 5f;

        public Transform DialogueContainer;

        public void SetDialogArray(string[] dialogueArray)
        {
            StartCoroutine(SpawnDialogue(dialogueArray));
        }

        private IEnumerator SpawnDialogue(string[] dialogueArray)
        {
            TransformUtil.ClearChilds(DialogueContainer);

            foreach (string dialogueString in dialogueArray)
            {
                var dialogue = Instantiate(DialogBubblePrefab, DialogueContainer);
                dialogue.SetDialog(dialogueString, DialogLimit);
                yield return new WaitForSeconds(DialogTime);
            }
        }
    }
}
