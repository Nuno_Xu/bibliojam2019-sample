﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.Dialog
{
    public class DialogSpawner : MonoBehaviour
    {
        [Serializable]
        public struct DialogData
        {
            public string[] Dialogue;
        }

        public DialogData[] DialogSetup;
        public float DialogInterval = 60f;
        public DialogList DialogList;

        private float _lastTime;
        private List<DialogData> _dialogueList;

        private void Start()
        {
            _lastTime = Time.time;
            _dialogueList = new List<DialogData>(DialogSetup);
        }

        private void Update()
        {
            if (_dialogueList == null || _dialogueList.Count == 0)
                return;

            if (Time.time - _lastTime >= DialogInterval)
            {
                SpawnRandomDialog();
                _lastTime = Time.time;
            }
        }

        private void SpawnRandomDialog()
        {
            if (_dialogueList == null || _dialogueList.Count == 0)
                return;

            var random = UnityEngine.Random.Range(0, _dialogueList.Count);
            var randomDialogue = _dialogueList[random];
            _dialogueList.RemoveAt(random);
            DialogList.SetDialogArray(randomDialogue.Dialogue);
        }
    }
}
