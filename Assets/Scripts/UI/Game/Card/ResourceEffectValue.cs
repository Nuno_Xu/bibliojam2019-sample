﻿using Assets.Scripts.Game;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.Game
{
    public enum NumberType
    {
        Positive,
        Negative,
        Neutral,
        Auto
    }

    [ExecuteInEditMode]
    public class ResourceEffectValue : MonoBehaviour
    {
        public ResourceIcon ResourceIcon;
        public TMP_Text CostValueText;

        public NumberType Type;

        [SerializeField]
        private ResourceType _resourceType;
        public ResourceType ResourceType
        {
            get { return _resourceType; }
            set
            {
                _resourceType = value;
                ResourceIcon.ResourceType = value;
            }
        }

        [SerializeField]
        private int _resourceValue;
        public int CostValue
        {
            get { return _resourceValue; }
            set
            {
                _resourceValue = value;
                SetResourceValue();
            }
        }

        private void SetResourceValue()
        {
            string value = "";
            if (Type == NumberType.Negative)
                value = "-";
            else if (Type == NumberType.Positive)
                value = "+";
            else if (Type == NumberType.Auto)
            {
                if (CostValue > 0)
                    value = "+";
            }

            CostValueText.text = value + CostValue;
        }


#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying && ResourceIcon != null)
            {
                ResourceIcon.ResourceType = ResourceType;
            }
        }
#endif
    }
}
