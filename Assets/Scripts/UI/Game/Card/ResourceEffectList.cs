﻿using Assets.Scripts.Game;
using Assets.Scripts.Util;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.Game
{
    public class ResourceEffectList : MonoBehaviour
    {
        public Transform Container;
        public ResourceEffectValue ResourceCostPrefab;

        public void SetEffect(Dictionary<ResourceType, int> costDictionary)
        {
            TransformUtil.ClearChilds(Container);

            foreach (KeyValuePair<ResourceType, int> costData in costDictionary)
            {
                var resourceCost = Instantiate(ResourceCostPrefab, Container);
                resourceCost.ResourceType = costData.Key;
                resourceCost.CostValue = costData.Value;
            }
        }
    }
}
