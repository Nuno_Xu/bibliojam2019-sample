﻿using Assets.Scripts.Game;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Game.Map;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Game
{
    [ExecuteInEditMode]
    public class BuildingCard : MonoBehaviour
    {
        public Image TileImage;
        public TileTypeSpriteData[] TileTypeSettingsArray;
        public ResourceEffectList ResourceCost;
        public ResourceEffectList ResourceEffect;
        public ResourceEffectList ResourceSell;

        [SerializeField]
        private TileType _tileType;
        public TileType TileType
        {
            get { return _tileType; }
            set
            {
                _tileType = value;
                SetTileType();
            }
        }

        [SerializeField]
        private Phase _phase;
        public Phase Phase
        {
            get { return _phase; }
            set
            {
                _phase = value;
                SetTileType();
            }
        }


        private void Start()
        {
            if (Application.isPlaying)
                SetTileType();
        }

        private void SetTileType()
        {
            SetTileTypeSprite();
            SetTileCostText();
            SetTileEffectText();
            SetTileSellText();
        }

        private void SetTileTypeSprite()
        {
            if (TileTypeSettingsArray == null)
                return;

            foreach (TileTypeSpriteData resourceTypeIcon in TileTypeSettingsArray)
            {
                if (resourceTypeIcon.Type == _tileType && (resourceTypeIcon.Phase == Phase.All || resourceTypeIcon.Phase == _phase))
                {
                    TileImage.sprite = resourceTypeIcon.Sprite;
                    break;
                }
            }
        }

        public void SelectBuilding()
        {
            if (!TutorialValid())
            {
                EventManager.Instance.TriggerEvent(EventManager.MUST_BUILD_FACTORY);
            }
            else if (PayCost())
            {
                BuildingSelector.Instance.SelectBuilding(TileType);
                if (LevelManager.Instance.TutorialActive)
                {
                    EventManager.Instance.TriggerEventDelayed(EventManager.EARTHQUAKE, 3f);
                    MapAnimator.Instance.SetAnimation(MapAnimator.SHAKING, true);
                }

                if (TileType == TileType.School && !LevelManager.Instance.Endless)
                    EventManager.Instance.TriggerEvent(EventManager.WIN_EVENT);
            }
            else
                EventManager.Instance.TriggerEvent(EventManager.NO_MONEY_EVENT);
        }



        public void SellBuilding()
        {
            if (!TutorialValid())
            {
                EventManager.Instance.TriggerEvent(EventManager.MUST_BUILD_FACTORY);
            }
            else
            {
                SellGet();
                BuildingSell.Instance.SellBuilding();
            }
        }

        public void SetTileCostText()
        {
            var cost = GetCost();
            ResourceCost.SetEffect(cost);
        }

        public void SetTileSellText()
        {
            if (ResourceSell != null)
            {
                var cost = GetSell();
                ResourceSell.SetEffect(cost);
            }
        }

        private void SellGet()
        {
            Dictionary<ResourceType, int> sell = GetSell();
            foreach (KeyValuePair<ResourceType, int> sellValue in sell)
            {
                LevelManager.Instance.ResourceManager.AddResource(sellValue.Key, sellValue.Value);
            }
        }

        private bool PayCost()
        {
            var cost = GetCost();
            return LevelManager.Instance.ResourceManager.TryPayCost(cost);
        }


        public void SetTileEffectText()
        {
            var income = GetIncome();
            ResourceEffect.SetEffect(income);
        }

        private Dictionary<ResourceType, int> GetCost()
        {
            return LevelManager.Instance.GetTileTypeCost(_tileType, _phase);
        }

        private Dictionary<ResourceType, int> GetSell()
        {
            return LevelManager.Instance.GetTileTypeSell(_tileType, _phase);
        }

        private Dictionary<ResourceType, int> GetIncome()
        {
            return LevelManager.Instance.GetTileTypeIncome(_tileType, _phase);
        }

        private bool TutorialValid()
        {
            return !LevelManager.Instance.TutorialActive || _tileType == TileType.Money;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                SetTileTypeSprite();
            }
        }
#endif
    }
}
