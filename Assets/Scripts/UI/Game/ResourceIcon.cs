﻿using Assets.Scripts.Game;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Game
{
    [ExecuteInEditMode]
    public class ResourceIcon : MonoBehaviour
    {
        [Serializable]
        public struct ResourceTypeIcon
        {
            public ResourceType Type;
            public Sprite Icon;
        }

        [SerializeField]
        private ResourceType _resourceType;
        public ResourceType ResourceType
        {
            get { return _resourceType; }
            set
            {
                _resourceType = value;
                SetResourceTypeIcon();
            }
        }


        public Image ResourceTypeImage;
        public ResourceTypeIcon[] ResourceTypeIconArray;

        public void Start()
        {
            SetResourceTypeIcon();
        }

        private void SetResourceTypeIcon()
        {
            if (ResourceTypeIconArray == null)
                return;

            foreach (ResourceTypeIcon resourceTypeIcon in ResourceTypeIconArray)
            {
                if (resourceTypeIcon.Type == _resourceType)
                {
                    ResourceTypeImage.sprite = resourceTypeIcon.Icon;
                    break;
                }
            }
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                SetResourceTypeIcon();
            }
        }
#endif
    }
}
