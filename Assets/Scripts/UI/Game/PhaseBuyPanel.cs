﻿using Assets.Scripts.Game;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Util;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Game
{
    [ExecuteInEditMode]
    public class PhaseBuyPanel : SingletonMonoBehaviour<PhaseBuyPanel>
    {
        [Serializable]
        public struct TowerSpriteData
        {
            public Phase Phase;
            public Sprite Sprite;
        }

        [SerializeField]
        private Phase _phase;
        public Phase Phase
        {
            get { return _phase; }
            set
            {
                _phase = value;
                SetTowerSprite();
                SetPhaseView();
                SetCost();
            }
        }

        [Serializable]
        public struct PhaseCostData
        {
            public Phase Phase;
            public ResourceType Resource;
            public int Cost;
        }

        public PhaseCostData[] PhaseCostDataArray;
        public TowerSpriteData[] TowerSpriteArray;
        public Image TowerSpriteImage;
        public ObjectToggler Modal;
        public ResourceEffectList CostList;

        public Transform BuyPanel;
        public Transform FinalPanel;

        private void Start()
        {
            SetTowerSprite();
            SetPhaseView();
            SetCost();
        }

        public void OpenPanel()
        {
            Modal.OpenPanel();
        }


        private bool PayCost()
        {
            var cost = GetCost();
            return LevelManager.Instance.ResourceManager.TryPayCost(cost);
        }

        public void PassPhase()
        {
            if (PayCost())
            {
                LevelManager.Instance.UpdateIncome();
                string targetEvent = null;
                if (_phase == Phase.Past)
                {
                    targetEvent = "GoToPresent";
                }
                else if (_phase == Phase.Present)
                {
                    targetEvent = "GoToFuture";
                }
                else
                {
                    return;
                }
                EventManager.Instance.TriggerEvent(targetEvent);
                Modal.ClosePanel();
            }
        }

        private void SetTowerSprite()
        {
            if (TowerSpriteArray == null)
                return;

            foreach (TowerSpriteData towerSprite in TowerSpriteArray)
            {
                if (towerSprite.Phase == _phase)
                {
                    TowerSpriteImage.sprite = towerSprite.Sprite;
                    break;
                }
            }
        }

        private void SetPhaseView()
        {
            BuyPanel.gameObject.SetActive(Phase != Phase.Future);
            FinalPanel.gameObject.SetActive(Phase == Phase.Future);
        }

        private void SetCost()
        {
            var cost = GetCost();
            CostList.SetEffect(cost);
        }

        public Dictionary<ResourceType, int> GetCost()
        {
            if (PhaseCostDataArray == null)
                return null;

            Dictionary<ResourceType, int> cost = new Dictionary<ResourceType, int>();
            foreach (PhaseCostData costData in PhaseCostDataArray)
            {
                if (costData.Phase == _phase)
                {
                    cost.Add(costData.Resource, costData.Cost);
                }
            }

            return cost;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                SetTowerSprite();
                SetPhaseView();
            }
        }
#endif
    }
}
