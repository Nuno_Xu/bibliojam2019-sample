﻿using Assets.Scripts.Game;
using Assets.Scripts.Game.Map;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.UI.Game
{
    public class BuildingSelector : SingletonMonoBehaviour<BuildingSelector>
    {
        public ObjectToggler Modal;
        public AudioSource BuildAudioSource;
        public BuildingCard[] BuildCardArray;
        private MapTile _selectedMapTile;


        public void OpenSelector(MapTile mapTile)
        {
            Modal.OpenPanel();
            _selectedMapTile = mapTile;
        }

        public void SelectBuilding(TileType tileType)
        {
            _selectedMapTile.TileType = tileType;
            BuildAudioSource.Play();
            LevelManager.Instance.UpdateIncome();
            Modal.ClosePanel();
        }
    }
}
