﻿using Assets.Scripts.Game;
using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Game
{
    public enum IncomeSpeed
    {
        Neutral,
        Negative3,
        Negative2,
        Negative1,
        Positive1,
        Positive2,
        Positive3,
    }

    [ExecuteInEditMode]
    public class ResourceBar : MonoBehaviour
    {
        [Serializable]
        public struct IncomeSpeedSprite
        {
            public Sprite Sprite;
            public IncomeSpeed IncomeSpeed;
        }


        [Serializable]
        public struct IncomeSpeedStep
        {
            public int Step;
            public IncomeSpeed IncomeSpeed;
        }

        private const string INCOME_FORMAT = "+{0}/s";
        [SerializeField]
        private ResourceType _resourceType;
        public ResourceType ResourceType
        {
            get { return _resourceType; }
            set
            {
                _resourceType = value;
                ResourceIcon.ResourceType = value;
                BindResourceValue();
                BindIncomeValue();
            }
        }

        [SerializeField]
        private IncomeSpeed _incomeSpeed;
        public IncomeSpeed IncomeSpeed
        {
            get { return _incomeSpeed; }
            set
            {
                _incomeSpeed = value;
                SetIncomeSpeed();
            }
        }


        public IncomeSpeedSprite[] IncomeSpeedSpriteArray;
        public IncomeSpeedStep[] IncomeSpeedStepArray;

        public Image IncomeSpeedImage;
        public ResourceIcon ResourceIcon;
        public Image ResourceValueBar;
        public TMP_Text IncomeValueText;

        private Action _valueBindingCallback;
        private Action _incomeBindingCallback;

        public void Start()
        {
            ResourceIcon.ResourceType = ResourceType;
            if (Application.isPlaying)
            {
                var resourceManager = LevelManager.Instance.ResourceManager;
                _valueBindingCallback = resourceManager.RegisterValueCallback(ResourceType, BindResourceValue);
                _incomeBindingCallback = resourceManager.RegisterIncomeCallback(ResourceType, BindIncomeValue);
            }
        }

        private void OnDestroy()
        {
            _valueBindingCallback?.Invoke();
            _incomeBindingCallback?.Invoke();
        }

        private void BindResourceValue()
        {
            var resourceManager = LevelManager.Instance.ResourceManager;
            var resourceValue = resourceManager.GetResource(ResourceType);
            float maxValue = resourceManager.GetMaxResource(ResourceType);

            ResourceValueBar.fillAmount = resourceValue / maxValue;
        }

        private void SetIncomeSpeed()
        {
            if (IncomeSpeedSpriteArray == null || IncomeSpeedImage == null)
                return;

            foreach (IncomeSpeedSprite speedSprite in IncomeSpeedSpriteArray)
            {
                if (speedSprite.IncomeSpeed == _incomeSpeed)
                {
                    IncomeSpeedImage.sprite = speedSprite.Sprite;
                    return;
                }
            }
        }

        private void BindIncomeValue()
        {
            if (IncomeSpeedStepArray == null)
                return;

            var incomeValue = LevelManager.Instance.ResourceManager.GetIncome(ResourceType);
            foreach (IncomeSpeedStep incomeSpeedStep in IncomeSpeedStepArray)
            {
                if (incomeValue <= incomeSpeedStep.Step)
                {
                    IncomeSpeed = incomeSpeedStep.IncomeSpeed;
                    return;
                }
            }
            IncomeSpeed = IncomeSpeedStepArray.Last().IncomeSpeed;
            //IncomeValueText.text = string.Format(INCOME_FORMAT, incomeValue);
        }
#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying && ResourceIcon != null)
            {
                ResourceIcon.ResourceType = ResourceType;
                SetIncomeSpeed();
            }
        }
#endif
    }
}
