﻿using Assets.Scripts.Game.Events;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.Game
{
    public class HintPanel : MonoBehaviour
    {
        public TMP_Text HintText;
        public Action _handleEventClose;

        public void CloseHint()
        {
            _handleEventClose?.Invoke();
        }

        public void ShowHint(LevelEvent currentEvent, Action handleEventClose)
        {
            _handleEventClose = handleEventClose;
            gameObject.SetActive(true);
            HintText.text = currentEvent.HintText;
        }
    }
}
