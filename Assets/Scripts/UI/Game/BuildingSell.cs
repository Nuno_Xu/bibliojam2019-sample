﻿using Assets.Scripts.Game;
using Assets.Scripts.Game.Map;
using Assets.Scripts.Util;

namespace Assets.Scripts.UI.Game
{
    public class BuildingSell : SingletonMonoBehaviour<BuildingSell>
    {
        public ObjectToggler Modal;
        public BuildingCard BuildCard;
        private MapTile _selectedMapTile;


        public void OpenSelector(MapTile mapTile)
        {
            Modal.OpenPanel();
            BuildCard.TileType = mapTile.TileType;
            BuildCard.Phase = mapTile.Phase;
            _selectedMapTile = mapTile;
        }

        public void SellBuilding()
        {
            _selectedMapTile.TileType = TileType.Empty;
            LevelManager.Instance.UpdateIncome();
            Modal.ClosePanel();
        }
    }
}
