﻿using Assets.Scripts.Game;
using Assets.Scripts.UI.Game;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.ViewModel
{
    [ExecuteInEditMode]
    public class ResourceViewer : MonoBehaviour
    {
        private const string INCOME_FORMAT = "{0}/s";
        private const string POSITIVE_INCOME_FORMAT = "+{0}/s";

        [SerializeField]
        private ResourceType _resourceType;
        public ResourceType ResourceType
        {
            get { return _resourceType; }
            set
            {
                _resourceType = value;
                ResourceIcon.ResourceType = value;
                BindResourceValue();
                BindIncomeValue();
            }
        }


        public ResourceIcon ResourceIcon;
        public TMP_Text ResourceValueText;
        public TMP_Text IncomeValueText;

        private Action _valueBindingCallback;
        private Action _incomeBindingCallback;

        public void Start()
        {
            ResourceIcon.ResourceType = ResourceType;
            if (Application.isPlaying)
            {
                var resourceManager = LevelManager.Instance.ResourceManager;
                _valueBindingCallback = resourceManager.RegisterValueCallback(ResourceType, BindResourceValue);
                _incomeBindingCallback = resourceManager.RegisterIncomeCallback(ResourceType, BindIncomeValue);
            }

        }

        private void OnDestroy()
        {
            _valueBindingCallback?.Invoke();
            _incomeBindingCallback?.Invoke();
        }

        private void BindResourceValue()
        {
            var resourceValue = LevelManager.Instance.ResourceManager.GetResource(ResourceType);
            ResourceValueText.text = resourceValue.ToString();
        }

        private void BindIncomeValue()
        {
            var incomeValue = LevelManager.Instance.ResourceManager.GetIncome(ResourceType);
            string format = incomeValue > 0 ? POSITIVE_INCOME_FORMAT : INCOME_FORMAT;
            IncomeValueText.text = string.Format(format, incomeValue);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying && ResourceIcon != null)
            {
                ResourceIcon.ResourceType = ResourceType;
            }
        }
#endif
    }
}
