﻿
using Assets.Scripts.Game;
using Assets.Scripts.Game.Events;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.Game
{
    public class ConsequencesPanel : MonoBehaviour
    {
        public TMP_Text ConsequenceText;
        public ResourceEffectList ResourceConsequences;

        private Action _handleEventClose;

        public void CloseConsequences()
        {
            _handleEventClose();
        }

        public void ShowConsequences(LevelEvent currentEvent, Action handleEventClose)
        {
            _handleEventClose = handleEventClose;
            gameObject.SetActive(true);
            ConsequenceText.text = currentEvent.ConsequencesText;
            if (currentEvent.ShowResourceConsequences)
            {
                var previousResources = currentEvent.PreviousResources;
                var currentResources = LevelManager.Instance.ResourceManager.GetAllResource();
                var diff = new Dictionary<ResourceType, int>();
                foreach (KeyValuePair<ResourceType, int> currentResourceData in currentResources)
                {
                    var type = currentResourceData.Key;
                    if (previousResources.ContainsKey(type))
                    {
                        var current = currentResourceData.Value;
                        var previous = previousResources[type];
                        if (previous != current)
                        {
                            diff[type] = current - previous;
                        }
                    }
                }

                if (diff.Count > 0)
                {
                    ResourceConsequences.SetEffect(diff);
                    ResourceConsequences.gameObject.SetActive(true);
                }
                else
                {
                    ResourceConsequences.gameObject.SetActive(false);
                }
            }
            else
            {
                ResourceConsequences.gameObject.SetActive(false);
            }
        }
    }
}
