﻿using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class LossPanel : MonoBehaviour
    {
        public ObjectToggler Panel;

        public void ShowLoss()
        {
            Panel.OpenPanel();
        }
    }
}
