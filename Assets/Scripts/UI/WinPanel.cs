﻿using Assets.Scripts.Game;
using Assets.Scripts.Game.Events;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class WinPanel : MonoBehaviour
    {
        public ObjectToggler Panel;
        public EventModal Event;

        public void ShowWin()
        {
            Panel.OpenPanel();
        }

        public void Continue()
        {
            LevelManager.Instance.Endless = true;
            Event.HandleEventClose();
        }
    }
}
