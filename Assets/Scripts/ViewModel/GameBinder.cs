﻿using UnityEngine;

namespace Assets.Scripts.ViewModel
{
    public abstract class GameBinder : MonoBehaviour
    {
        public abstract void Bind();
        public abstract void Unbind();

        private void OnDestroy()
        {
            Unbind();
        }
    }
}
