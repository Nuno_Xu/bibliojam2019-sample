﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.ViewModel
{
    public class PropertyBinder<T>
        where T : IEquatable<T>
    {
        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                if ((_value == null && value != null) || !_value.Equals(value))
                {
                    SetValue(value);
                }
            }
        }

        private List<Action> _bindingList = new List<Action>();

        public Action Bind(Action action)
        {
            _bindingList.Add(action);
            return () => _bindingList.Remove(action);
        }

        private void SetValue(T value)
        {
            _value = value;
            CallBindings();
        }

        private void CallBindings()
        {
            foreach (Action action in _bindingList)
            {
                action?.Invoke();
            }
        }
    }
}
