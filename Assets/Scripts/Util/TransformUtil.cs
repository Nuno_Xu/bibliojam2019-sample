﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Util
{
    public static class TransformUtil
    {
        public static void ClearChilds(Transform parent)
        {
            var childCount = parent.childCount;
            List<Transform> childList = new List<Transform>();
            for (int i = 0; i < childCount; i++)
            {
                childList.Add(parent.GetChild(i));
            }
            foreach (Transform child in childList)
            {
                if (Application.isPlaying)
                    Object.Destroy(child.gameObject);
                else
                    Object.DestroyImmediate(child.gameObject);
            }
        }
    }
}
